# Advaced CSS and Sass
***

## 1. Install Sass locally
***

1. <https://nodejs.org>
    - cli 명령어로 node.js 버전 확인 : `node -v`
1. 프로젝트 폴더로 이동 : `cd 폴더명`
1. npm start : `npm init`
1. package download
    - node-sass : `npm install node-sass --save-dev`
    - live-server : `npm install live-server -g`
        - automatically reloading a page on file changes (global install)
    - `npm install` : package.json 파일에 저장된 모듈 자동으로 세팅


##2. Sass
***
https://devhints.io/sass

## 3. Flexbox
***
https://codepen.io/osublake/full/dMLQJr
https://developer.mozilla.org/ko/docs/Web/CSS/CSS_Flexible_Box_Layout/Flexbox%EC%9D%98_%EA%B8%B0%EB%B3%B8_%EA%B0%9C%EB%85%90

## 4. Grid layouts
***

## 5. Responsive Design
***